# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### [0.0.27](https://stash.tcsbank.ru/projects/TCRM/repos/tcrmf-service/compare/commits?targetBranch=refs%2Ftags%2@lerna-sandbox/second-library@0.0.26&sourceBranch=refs%2Ftags%2F@lerna-sandbox/second-library@0.0.27) (2020-09-03)

**Note:** Version bump only for package @lerna-sandbox/second-library





### [0.0.26](https://stash.tcsbank.ru/projects/TCRM/repos/tcrmf-service/compare/commits?targetBranch=refs%2Ftags%2@lerna-sandbox/second-library@0.0.25&sourceBranch=refs%2Ftags%2F@lerna-sandbox/second-library@0.0.26) (2020-09-01)

**Note:** Version bump only for package @lerna-sandbox/second-library





### [0.0.25](https://stash.tcsbank.ru/projects/TCRM/repos/tcrmf-service/compare/commits?targetBranch=refs%2Ftags%2@lerna-sandbox/second-library@0.0.24&sourceBranch=refs%2Ftags%2F@lerna-sandbox/second-library@0.0.25) (2020-08-07)

**Note:** Version bump only for package @lerna-sandbox/second-library





### [0.0.24](https://stash.tcsbank.ru/projects/TCRM/repos/tcrmf-service/compare/commits?targetBranch=refs%2Ftags%2@lerna-sandbox/second-library@0.0.23&sourceBranch=refs%2Ftags%2F@lerna-sandbox/second-library@0.0.24) (2020-08-07)

**Note:** Version bump only for package @lerna-sandbox/second-library





## <small>0.0.23 (2020-08-07)</small>

**Note:** Version bump only for package @lerna-sandbox/second-library





## <small>0.0.22 (2020-08-07)</small>

**Note:** Version bump only for package @lerna-sandbox/second-library





## <small>0.0.21 (2020-08-07)</small>

**Note:** Version bump only for package @lerna-sandbox/second-library





## <small>0.0.20 (2020-08-07)</small>

**Note:** Version bump only for package @lerna-sandbox/second-library





## <small>0.0.19 (2020-08-07)</small>

**Note:** Version bump only for package @lerna-sandbox/second-library





## <small>0.0.18 (2020-08-07)</small>

**Note:** Version bump only for package @lerna-sandbox/second-library





## <small>0.0.17 (2020-08-07)</small>

**Note:** Version bump only for package @lerna-sandbox/second-library





## <small>0.0.16 (2020-08-07)</small>

**Note:** Version bump only for package @lerna-sandbox/second-library





## <small>0.0.15 (2020-08-07)</small>

**Note:** Version bump only for package @lerna-sandbox/second-library





## <small>0.0.14 (2020-08-06)</small>

* fix(first-library): changed selector 14 ([e607ec1](https://bitbucket.org/infernal_apel/lerna-sandbox/commits/e607ec1))
