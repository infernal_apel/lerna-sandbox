import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FirstLibraryComponent } from './first-library.component';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule],
  declarations: [FirstLibraryComponent]
})
export class FirstLibraryModule {}
