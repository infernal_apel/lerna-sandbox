# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

### 0.0.26 (2020-09-03)

**Note:** Version bump only for package @lerna-sandbox/third-library





### [0.0.25](https://stash.tcsbank.ru/projects/TCRM/repos/tcrmf-service/compare/commits?targetBranch=refs%2Ftags%2@lerna-sandbox/first-library@0.0.24&sourceBranch=refs%2Ftags%2F@lerna-sandbox/first-library@0.0.25) (2020-09-01)

**Note:** Version bump only for package @lerna-sandbox/first-library





### [0.0.24](https://stash.tcsbank.ru/projects/TCRM/repos/tcrmf-service/compare/commits?targetBranch=refs%2Ftags%2@lerna-sandbox/first-library@0.0.23&sourceBranch=refs%2Ftags%2F@lerna-sandbox/first-library@0.0.24) (2020-08-07)


### Bug Fixes

* **first-library:** component selector change 1231231232244 ([74a075c](https://stash.tcsbank.ru/projects/TCRM/repos/tcrmf-service/commits/74a075c))



### [0.0.23](https://stash.tcsbank.ru/projects/TCRM/repos/tcrmf-service/compare/commits?targetBranch=refs%2Ftags%2@lerna-sandbox/first-library@0.0.22&sourceBranch=refs%2Ftags%2F@lerna-sandbox/first-library@0.0.23) (2020-08-07)


### Bug Fixes

* **first-library:** component selector change 1231231232244 ([6d1d2c7](https://stash.tcsbank.ru/projects/TCRM/repos/tcrmf-service/commits/6d1d2c7))



## <small>0.0.22 (2020-08-07)</small>

* fix(first-library): component selector change 1231231232244 ([b1b377c](https://bitbucket.org/infernal_apel/lerna-sandbox/commits/b1b377c))





## <small>0.0.21 (2020-08-07)</small>

* fix(first-library): component selector change 123123123224 ([7fd4a3d](https://bitbucket.org/infernal_apel/lerna-sandbox/commits/7fd4a3d))





## <small>0.0.20 (2020-08-07)</small>

* fix(first-library): component selector change 123123123445 ([f188bd3](https://bitbucket.org/infernal_apel/lerna-sandbox/commits/f188bd3))





## <small>0.0.19 (2020-08-07)</small>

* fix(first-library): component selector change 12312312322 ([184d7ce](https://bitbucket.org/infernal_apel/lerna-sandbox/commits/184d7ce))





## <small>0.0.18 (2020-08-07)</small>

* fix(first-library): component selector change 12312312322 ([c4e89c8](https://bitbucket.org/infernal_apel/lerna-sandbox/commits/c4e89c8))





## <small>0.0.17 (2020-08-07)</small>

* fix(first-library): component selector change 12312312344 ([c892376](https://bitbucket.org/infernal_apel/lerna-sandbox/commits/c892376))





## <small>0.0.16 (2020-08-07)</small>

* fix(first-library): component selector change 1231231232 ([66deaa4](https://bitbucket.org/infernal_apel/lerna-sandbox/commits/66deaa4))





## <small>0.0.15 (2020-08-07)</small>

* fix(first-library): component selector change 1231231234 ([ed79c01](https://bitbucket.org/infernal_apel/lerna-sandbox/commits/ed79c01))





## <small>0.0.14 (2020-08-07)</small>

* fix(first-library): component selector change 123123123 ([39a26b4](https://bitbucket.org/infernal_apel/lerna-sandbox/commits/39a26b4))





## <small>0.0.13 (2020-08-06)</small>

* fix(first-library): changed selector 14 ([e607ec1](https://bitbucket.org/infernal_apel/lerna-sandbox/commits/e607ec1))
